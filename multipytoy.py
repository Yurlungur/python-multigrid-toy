#!/usr/bin/env python
# Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
# Author: Trevor Vincent

import numpy as np
from copy import copy
import matplotlib.pyplot as plt
from numpy import linalg
from scipy.interpolate import BarycentricInterpolator

xmin = 0
xmax = 1
num_smoother_steps=3
lowest_level=1
j0 = 5

def rho(x):
    return np.array([1 if 0.25 <= xi <= 0.75 else 0 for xi in x])

def smoother_step(y,b,x):
    print x
    dx = x[1]-x[0]
    out = np.empty_like(y)
    out[1:-1] = (b[1:-1] - ((y[2:] + y[:-2])/(dx*dx)))/(-2.*dx*dx)
    out[0] = 0
    out[-1] = 0
    return out

def smooth(y,b,x,times):
    out = copy(y)
    for i in range(times):
        out = smoother_step(out,b,x)
    return out

def restriction(y,x):
    j = (len(y) -1)/2
    print "j = {}".format(j)
    j_new = j - 1
    print "j_fine = {}".format(j_new)
    n_new = 2*j_new + 1
    x_coarse = np.linspace(xmin,xmax,n_new)
    y_coarse = np.empty(n_new)
    for i in range(1,len(y_coarse)-1):
        y_coarse[i] = (y[2*i-1] + 2*y[2*i] + y[2*i+1])/4.
    y_coarse[0] = y[0]
    y_coarse[-1] = y[-1]
    return x_coarse,y_coarse

def prolongation(y,x):
    interpolator = BarycentricInterpolator(x,y)
    j = (len(y)-1)/2
    j_new = j + 1
    n_new = 2*j_new+1
    x_fine = np.linspace(xmin,xmax,n_new)
    y_fine = interpolator(x_fine)
    return y_fine

def residual(y,b,x):
    dx = x[1]-x[0]
    out = np.empty_like(y)
    out[1:-1] = (y[2:]-2*y[1:-1] + y[:-2])/(dx*dx) - b[1:-1]
    out[0] = 0
    out[-1] = 0
    return out

def coarse_solve(y,x,rhs):
    dx = x[1]-x[0]
    A = np.zeros((len(x),len(x)))
    for i in range(1,len(x)-1):
        A[i,i-1] = 1.
        A[i,i+1] = 1.
        A[i,i] = -2.
    A[0,0] = -2.
    A[0,1] = 1.
    A[-1,-1] = -2.
    A[-1,-2] = 1
    A /= (dx*dx)
    print A
    return linalg.solve(A,rhs)

def vcycle(j,y,x,rhs):
    nf = len(y)
    nc = (nf+1)/2
    y_smooth = smooth(y,rhs,x,num_smoother_steps)
    if j==2*lowest_level+1:
        y = coarse_solve(y,x,rhs)
    else:
        res = residual(y_smooth,rhs,x)
        x_restricted,res_restricted = restriction(res,x)
        error = np.zeros_like(res_restricted)
        error = vcycle(j-1,error,x_restricted,res_restricted)
        print y.shape,error.shape,x_restricted.shape
        p = prolongation(error,x_restricted)
        print p.shape
        print j
        y += p
    return smooth(y,rhs,x,num_smoother_steps)

def solve():
    n = 2*j0+1
    x0 = np.linspace(xmin,xmax,n)
    y0 = np.zeros_like(x0)
    return vcycle(j0,y0,x0,rho(x0))


solution = solve()
x = np.linspace(xmin,xmax,2*j0+1)
plt.plot(x,solution)
plt.show()
